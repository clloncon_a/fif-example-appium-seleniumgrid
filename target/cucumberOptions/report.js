$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Login.feature");
formatter.feature({
  "line": 2,
  "name": "Login",
  "description": "",
  "id": "login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@release"
    },
    {
      "line": 1,
      "name": "@login"
    }
  ]
});
formatter.scenarioOutline({
  "line": 4,
  "name": "First Login of a client with valid ID",
  "description": "",
  "id": "login;first-login-of-a-client-with-valid-id",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I login the app with my \u003cIdNumber\u003e and \u003cPassword\u003e",
  "keyword": "Given "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "login;first-login-of-a-client-with-valid-id;",
  "rows": [
    {
      "cells": [
        "IdNumber",
        "Password"
      ],
      "line": 9,
      "id": "login;first-login-of-a-client-with-valid-id;;1"
    },
    {
      "cells": [
        "\"2706972K\"",
        "\"000111\""
      ],
      "line": 10,
      "id": "login;first-login-of-a-client-with-valid-id;;2"
    },
    {
      "cells": [
        "\"15105135\"",
        "\"000111\""
      ],
      "line": 11,
      "id": "login;first-login-of-a-client-with-valid-id;;3"
    },
    {
      "cells": [
        "\"233860786\"",
        "\"000111\""
      ],
      "line": 12,
      "id": "login;first-login-of-a-client-with-valid-id;;4"
    }
  ],
  "keyword": "Examples"
});
