package stepDefinitions;

import appPage.LoginPage;
import appPage.platformPage.LoginPageAndroid;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import utilities.BaseCustomer;
import utilities.Constant;
import utilities.Hooks;

public class LoginSteps {

    private BaseCustomer base;

    public LoginSteps(BaseCustomer base) {
        this.base = base;
    }

    protected MobileDriver<MobileElement> driver = null;
    private LoginPage loginPage;

    @Before
    public void beforeScenario() throws Throwable {
        new Hooks(base);
        driver = Hooks.getDriver();

        switch (Constant.PLATFORM.toLowerCase()) {
            case "android":
                loginPage = new LoginPageAndroid(driver);
                break;

            case "ios" :


                break;
        }
    }

    @After
    public void afterScenario() {
        Hooks.destroyDriver();
    }


    @Given("^I login the app with my \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_login_the_app_with_my_and(String arg1, String arg2) throws Throwable {

    }
}
