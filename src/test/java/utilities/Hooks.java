package utilities;


import java.io.File;
import java.net.URL;




import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Hooks extends BaseCustomer {
    public static MobileDriver<MobileElement> driver = null;

    private static DesiredCapabilities capabilities;
    private static String OS;


    @SuppressWarnings("unused")
    private BaseCustomer base;


    public Hooks(BaseCustomer base) throws Exception {
        initialize();
        this.base = base;
    }


    public static void initialize() throws Exception {
        if (driver == null) {
            try {
                driverInstance(Constant.PLATFORM);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    //Para no resetear telefono en cada prueba >> capabilities.setCapability("noReset","true");
    private static void settingDriverCapabilitiesAndroid() {
        File classPathRoot = new File(Constant.USER_DIR);
        File appDir = new File(classPathRoot, Constant.CLASS_PATH_ROOT);
        File app = new File(appDir, Constant.ANDROID_APP_NAME);
        capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", Constant.PLATFORM);
        capabilities.setCapability("platformVersion", Constant.PLATFORM_VERSION);
        //capabilities.setCapability("appActivity", Constant.APP_ACTIVITY);
        //capabilities.setCapability("appWaitActivity", Constant.APP_WAIT_ACTIVITY);
        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("clearSystemFiles", true);
        platformVersionCapabilities(Constant.PLATFORM_VERSION);
    }

    private static void platformVersionCapabilities(String PLATFORM_VERSION) {

        switch (PLATFORM_VERSION) {
            case "7.0":
                capabilities.setCapability("deviceName", "AndroidEmulator7.0");
                capabilities.setCapability("automationName", "uiautomator2");
                break;
            case "8.0":
                capabilities.setCapability("deviceName", "AndroidEmulator8.0");
                capabilities.setCapability("automationName", "uiautomator2");
                break;
        }
    }


    public static MobileDriver<MobileElement> getDriver() {
        return driver;
    }

    public static void destroyDriver() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }


    private static void driverInstance(String OS) throws Exception {
        try {
            //Driver Instance
            switch (OS.toLowerCase()) {
                case "android":
                    settingDriverCapabilitiesAndroid();
                    driver = new AndroidDriver<>(new URL(Constant.URL), capabilities);
                    break;
                case "ios":
                    break;
            }
        } catch (Exception e) {
            throw new Exception("Error al crear instancia del driver: " + e.getMessage());
        }
    }

}
