package utilities;

import java.time.Duration;

 public class Constant {

    public static final String PLATFORM = LoadProperties.getInstance().getPlatform();
    static final String PLATFORM_VERSION = LoadProperties.getInstance().getPlatformVersion();
    static final String URL = "http://127.0.0.1:4723/wd/hub";
    static final String CLASS_PATH_ROOT = "src/test/java/resources/";
    static final String ANDROID_APP_NAME =  "app-debug.apk";
    static final String APP_ACTIVITY = "test.common.tech.fif.com.myapplication.MyActivity";
    //static final String APP_WAIT_ACTIVITY = "net.technisys.android.core.gui.ui.SplashScreen";
    static final String USER_DIR = System.getProperty("user.dir");
    public static Duration WAIT_UNTIL_DECORATOR = Duration.ofSeconds(60);
}
