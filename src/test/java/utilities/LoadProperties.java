package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoadProperties {

    private static final String PLATFORM_VERSION = "PLATFORM_VERSION";
    private static final String PLATFORM = "PLATFORM";
    private static final String OS = "OS";
    private static Properties properties;
    private static InputStream inputStream;
    private static LoadProperties instance;

    private LoadProperties() {
        properties = new Properties();
        try {
            inputStream = new FileInputStream("config.properties");
        } catch (FileNotFoundException inputStreamException) {
            inputStreamException.printStackTrace();
        }
        if (null != inputStream) try {
            properties.load(inputStream);
        } catch (IOException loadException) {
            loadException.printStackTrace();
        }
    }

     static LoadProperties getInstance() {
        if (null == instance) instance = new LoadProperties();
        return instance;
    }

    public String getPlatformVersion() {
        return properties.getProperty(PLATFORM_VERSION);
    }
    
    public String getPlatform() {
        return properties.getProperty(PLATFORM);
    }
    
    public String getOS() {
        return properties.getProperty(OS);
    }
    
}