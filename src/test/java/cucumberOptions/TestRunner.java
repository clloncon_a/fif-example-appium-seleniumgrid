package cucumberOptions;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty", "html:target/cucumberOptions"},
        features = "src/test/java/features",
        glue = "stepDefinitions",
        dryRun = false,
        monochrome = true,
        tags = {"@login"}
        )

public class TestRunner {

}
