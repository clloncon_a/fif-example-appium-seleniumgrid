@release @login
Feature: Login

  Scenario Outline: First Login of a client with valid ID
    Given I login the app with my <IdNumber> and <Password>


    Examples:
      | IdNumber    | Password |
      | "2706972K"  | "000111" |
      | "15105135"  | "000111" |
      | "233860786" | "000111" |