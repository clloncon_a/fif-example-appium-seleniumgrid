package appPage.platformPage;

import appPage.LoginPage;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import static utilities.Constant.WAIT_UNTIL_DECORATOR;

public class LoginPageAndroid implements LoginPage {

    public MobileDriver<MobileElement> driver;

    public LoginPageAndroid(MobileDriver<MobileElement> driverInit) {
        driver = driverInit;
        PageFactory.initElements(new AppiumFieldDecorator(driver, WAIT_UNTIL_DECORATOR), this);
    }
}
